/**
 * Thread safe linked list library.
 */
#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct Element_
{
    void *data;
    struct Element_ *next;
} Element;

typedef struct List
{
    int size;
    pthread_mutex_t *list_lock;
    int (*comp)(const void *key1, const void *key2);
    void (*destroy)(void *data);
    Element *head;
    Element *tail;
} List;

/**
 * Initiliazes list structure with default values.
 * 
 * @param list Pointer to struct of type List.
 * @param destroy User provided function pointer to
 *     function to free data field within Element struct.
 * @return 0 on success, -1 if mutex lock init fails.
 */
int list_init(List *list, void (*destroy)(void *data));

/**
 * Iterates over each element in linked list freeing resources.
 * 
 * @param list Pointer to struct of type list.
 * @returns 0
 */
int list_destroy(List *list);

/**
 * Inserts data into linked list after element.
 * 
 * @param list Pointer to struct of type list.
 * @param element Pointer to struct of type Element.
 * @param data User data encapsulated into element then added
 *     to linked list.
 * @returns 0 on success, -1 if malloc fails.
 */
int list_ins_next(List *list, Element *element, const void *data);

/**
 * Removes and frees element occurring after element given as 
 * argument.  Returns removed elements data through parameter.
 * 
 * @param list Pointer to struct of type list.
 * @param element Pointer to struct of type element.
 * @param data Pointer to storage for removed elements data.
 */
int list_rem_next(List *list, Element *element, void **data);
#endif /* LINKED_LIST_H */
