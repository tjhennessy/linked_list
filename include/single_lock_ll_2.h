/**
 * This list is designed for use as a member of
 * a hash table.
 */
#ifndef SINGLE_LOCK_LL_2_H
#define SINGLE_LOCK_LL_2_H

typedef struct node_t_
{
    void *key;
    struct node_t_ *next;
} node_t;

typedef struct list_t_
{
    node_t *head;
    int (*match)(void *key1, void *key2);
    int (*destroy_node)(void *key);
    pthread_mutex_t lock;
} list_t;
    

int list_init(list_t *l, 
              int (*match)(void *key1, void *key2),
              int (*destroy_node)(void *key));
int list_insert(list_t *l, void *key);
int list_destroy(list_t *l);
int list_lookup(list_t *l, void *key);
#endif /* SINGLED_LOCK_LL_2_H */
