if [ -d "build/" ]; then
    rm -rf build/
fi
cmake -H. -Bbuild
cd build/
make -j 8
