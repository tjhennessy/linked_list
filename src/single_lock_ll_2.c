/**
 * List makes use of one lock for all critical
 * sections in linked list.  Hand-over-hand 
 * lock granularity does not perform much better
 * in practice than this implementation.
 *
 * compile using -pthread
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "single_lock_ll_2.h"

int list_init(list_t *l, 
              int (*match)(void *key1, void *key2),
              int (*destroy_node)(void *key))
{
    l->head = NULL;
    l->match = match;
    l->destroy_node = destroy_node;
    pthread_mutex_init(&l->lock, NULL);
    
    return 0;
}

int list_insert(list_t *l, void *key)
{
    node_t *new = (node_t *) malloc(sizeof(node_t));
    if (NULL == new)
    {
        perror("malloc");
        return -1;
    }
    new->key = key;

    pthread_mutex_lock(&l->lock);
    // ---begin critical section
    new->next = l->head;
    l->head = new;
    pthread_mutex_unlock(&l->lock);

    return 0;
}

int list_destroy(list_t *l)
{
    pthread_mutex_lock(&l->lock);
    node_t *curr = l->head;
    node_t *old;
    while (curr)
    {
        if (l->destroy_node)
        {
            l->destroy_node(curr->key);
        }
        old = curr;
        curr = curr->next;
        free(old);
    }
    pthread_mutex_unlock(&l->lock);
    pthread_mutex_destroy(&l->lock);
    memset(l, 0, sizeof(list_t));   
    
    return 0;
}

int list_lookup(list_t *l, void *key)
{
    int rv = -1;
    pthread_mutex_lock(&l->lock);
    node_t *curr = l->head;
    while (curr)
    {
        if (l->match(curr->key, key))
        {
            rv = 0;
            break;
        }
        curr = curr->next;
    }
    pthread_mutex_unlock(&l->lock);
    return rv;
}
