#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "single_lock_ll.h"

#define list_size(LIST)             ((LIST)->size)
#define list_head(LIST)             ((LIST)->head)
#define list_tail(LIST)             ((LIST)->tail)
#define list_is_head(LIST, ELEMENT) (((LIST)->head == ELEMENT) ? 1 : 0)
#define list_is_tail(ELEMENT)       ((NULL == (ELEMENT)->next) ? 1 : 0)
#define list_data(ELEMENT)          ((ELEMENT)->data)
#define list_next(ELEMENT)          ((ELEMENT)->next)

int list_init(List *list, void (*destroy)(void *data))
{
    int rv = pthread_mutex_init(list->list_lock, NULL);
    if (rv != 0)
    {
        fprintf(stderr, "Error: could not init lock\n");
        return -1;
    }

    list->size = 0;
    list->destroy = destroy;
    list->comp = NULL;
    list->head = NULL;
    list->tail = NULL;

    return 0;
}

int list_destroy(List *list)
{
    Element *old_elment;
    // ---begin critical section
    pthread_mutex_lock(list->list_lock);
    while (list_head(list) != NULL)
    {
        void *data = (void *) list_data(list_head(list));
        if (list->destroy != NULL)
        {   // Only called if user provided during init
            list->destroy(data);
        }
        old_elment = list_head(list);
        list->head = list_next(list_head(list));
        free(old_elment);
    }
    pthread_mutex_unlock(list->list_lock);
    // ---end of critical section
    pthread_mutex_destroy(list->list_lock);
    memset(list, 0, sizeof(List));

    return 0;
}

int list_ins_next(List *list, Element *element, const void *data)
{
    Element *new_element;
    new_element = (Element *) malloc(sizeof(Element));
    if (NULL == new_element)
    {
        fprintf(stderr, "Error: malloc failed\n");
        return -1;
    }

    new_element->data = (void *) data;

    // ---begin critical section
    pthread_mutex_lock(list->list_lock);
    // Insert element into list
    if (NULL == element)
    {   // NULL means no element comes before 
        // Insert at head of list
        if (list_size(list) == 0)
        {   // Is this first element in list?
            list->tail = new_element;
        }

        new_element->next = list->head;
        list->head = new_element;
    }
    else
    {
        // Insert somewhere besides head
        if (NULL == element->next)
        {   // Insert at tail of list
            list->tail = new_element;
        }

        new_element->next = element->next;
        element->next = new_element;
    }

    list->size++;
    // ---end critical section
    pthread_mutex_unlock(list->list_lock);

    return 0;
}

int list_rem_next(List *list, Element *element, void **data)
{
    Element *old_element;

    // ---begin critical section
    pthread_mutex_lock(list->list_lock);
    if (list_size(list) == 0)
    {
        pthread_mutex_unlock(list->list_lock);
        return -1;
    }

    if (NULL == element)
    {
        // Remove first element in list (at head)
        *data = list->head->data;
        old_element = list->head;
        list->head = list->head->next;

        if (list_size(list) == 1)
        {
            list->tail = NULL;
        }
    }
    else
    {
        if (NULL == element->next)
        {
            pthread_mutex_unlock(list->list_lock);
            return -1;
        }

        *data = element->next->data;
        old_element = element->next;
        element->next = element->next->next;

        if (NULL == element->next)
        {
            list->tail = element;
        }
    }
    
    free(old_element);

    list->size--;
    // ---end critical section
    pthread_mutex_unlock(list->list_lock);

    return 0;
}
